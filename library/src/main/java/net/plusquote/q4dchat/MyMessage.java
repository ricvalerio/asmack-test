package net.plusquote.q4dchat;

import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.format.DateUtils;
import android.text.style.ImageSpan;
import android.util.Log;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.delay.packet.DelayInformation;

import java.util.Date;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 09-12-2014.
 */
public class MyMessage {
    private Message message;
    private Date date;
    private String imageUrl;
    private Spanned body;

    public MyMessage(Message message) {
        this.message = message;
        this.date = extractDate();

        extractBodyAndImage();
    }

    private void extractBodyAndImage() {
        Spanned text = Html.fromHtml(getBody());
        SpannableStringBuilder spannable = new SpannableStringBuilder(text);
        ImageSpan[] imageSpans = getImageSpans(spannable, ImageSpan.class);

        String imageUrl = null;
        for(int i=0; i<imageSpans.length; i++) {
            String url = imageSpans[i].getSource();

            if(url != null && !url.isEmpty()) {
                imageUrl = url;
            }

            spannable.removeSpan(imageSpans[i]);
        }

        this.imageUrl = imageUrl;
        this.body = new SpannedString(spannable);
    }


    public Message getMessage() {
        return message;
    }

    public String getPlayerId() {
        return getMessage().getFrom().split("/")[1];
    }

    public String getFrom() {
        return getMessage().getFrom().split("/")[1];
    }

    public String getBody() {
        return message.getBody();
    }

    public Spanned getSpannedBody() {
        return body;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getAvatarUrl() {
        return "";
    }

    private Date extractDate() {
        Date date = null;
        DelayInformation inf = null;

        try {
            inf = message.getExtension("delay", "urn:xmpp:delay");
            date = inf.getStamp();
        } catch (Exception e) {
            date = new Date();
        }

        return date;
    }

    public Date getDate() {
        return date;
    }

    public CharSequence getHowLongAgo() {
        Date date = getDate();

        return DateUtils.getRelativeTimeSpanString(date.getTime());
    }


    public ImageSpan[] getImageSpans(Spanned original, Class<ImageSpan> sourceType) {
        SpannableStringBuilder result = new SpannableStringBuilder(original);
        ImageSpan[] spans = result.getSpans(0, result.length(), sourceType);

        return spans;
    }

}

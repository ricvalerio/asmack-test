package net.plusquote.q4dchat;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 05-12-2014.
 */
public class Config {
    public static final String HOST = "xmpp.test.quiz4deal.com";
    public static final int PORT = 5222;
    public static final String SERVICE = "xmpp.test.quiz4deal.com";

}

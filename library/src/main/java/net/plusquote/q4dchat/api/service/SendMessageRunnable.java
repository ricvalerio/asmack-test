package net.plusquote.q4dchat.api.service;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 08-12-2014.
 */
public class SendMessageRunnable implements Runnable {
    private XMPPTCPConnection connection;
    private MultiUserChat chat;
    private String text;
    private SendMessageCallback listener;

    public interface SendMessageCallback {
        public void sent();
        public void failed();
    }

    public SendMessageRunnable(XMPPTCPConnection connection, MultiUserChat chat, String text, SendMessageCallback listener) {
        this.connection = connection;
        this.chat = chat;
        this.text = text;
        this.listener = listener;
    }

    @Override
    public void run() {
        Message message = chat.createMessage();
        message.setBody(text);

        try {
            chat.sendMessage(message);
            if(listener != null)
                listener.sent();
        } catch (XMPPException e) {
            if(listener != null)
                listener.failed();

            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            if(listener != null)
                listener.failed();

            e.printStackTrace();
        }
    }
}

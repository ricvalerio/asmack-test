package net.plusquote.q4dchat.api;

import net.plusquote.q4dchat.api.service.ConnectRunnable;
import net.plusquote.q4dchat.api.service.ListChatsRunnable;
import net.plusquote.q4dchat.api.service.ListGroupsRunnable;
import net.plusquote.q4dchat.api.service.SendMessageRunnable;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.ping.PingManager;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 05-12-2014.
 */
public class Q4DChatApi {
    private String host;
    private int port;
    private String service;
    private String screenname;

    private XMPPTCPConnection connection;
    private ConnectionListener connectListener;

    private ArrayList<MultiUserChat> chats;

    private ExecutorService executor;

    public Q4DChatApi() {
        executor = Executors.newSingleThreadExecutor();
    }

    /**
     * setups the host, port and service to use when creating the connection
     * @param host
     * @param port
     * @param service
     * @return
     */
    public Q4DChatApi setup(String host, int port, String service) {
        this.host = host;
        this.port = port;
        this.service = service;

        return this;
    }

    /**
     * defines the connection listener, that will be set when creating a new connection
     * this does not have immediate effect when setting, only applies when doing connect()
     * @param listener
     * @return
     */
    public Q4DChatApi setOnConnectListener(ConnectionListener listener) {
        this.connectListener = listener;

        return this;
    }

    /**
     * sets the connection listener to the connection
     * @param listener
     */
    public void setListener(ConnectionListener listener) {
        if(listener != null)
            connection.addConnectionListener(listener);
    }

    /**
     * returns true if the user is connected and authenticated
     * @return
     */
    public boolean isConnected() {
        return connection != null && connection.isConnected() && connection.isAuthenticated();
    }

    /**
     * connects the user to the server, and attempts to authenticate
     * @param username
     * @param password
     */
    public void connect(String username, String password, String screenname) {
        connection = createConnection(username, password);

        this.screenname = screenname;

        PingManager.getInstanceFor(connection).setPingInterval(10);
        ReconnectionManager.getInstanceFor(connection).enableAutomaticReconnection();

        setListener(connectListener);

        connect();
    }


    /*
     * private methods
     */
    private XMPPTCPConnection createConnection(String username, String password) {
        XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                .setHost(host)
                .setPort(port)
                .setServiceName(service)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                .setLegacySessionDisabled(false)
                .setUsernameAndPassword(username, password)
                .setDebuggerEnabled(true)
                .build();

        return new XMPPTCPConnection(config);
    }


    private void connect() {
        executor.submit(new ConnectRunnable(connection));
    }



    /**
     * get the list of all the roster groups
     * @param listener
     */
    public void listRooms(final ListGroupsRunnable.ListRoomsCallback listener) {
        executor.submit(new ListGroupsRunnable(connection, service, new ListGroupsRunnable.ListRoomsCallback() {
            @Override
            public void handleList(final ArrayList<HostedRoom> rooms) {
                if (listener != null) {
                    listener.handleList(rooms);
                }
            }
        }));
    }

    /**
     * gets the list of MultiUserChats
     * @param listener
     */
    public void getChatRooms(final ListChatsRunnable.ListChatsCallback listener) {
        executor.submit(new ListChatsRunnable(connection, service, screenname, new ListChatsRunnable.ListChatsCallback() {
            @Override
            public void handleList(final ArrayList<MultiUserChat> mucs) {
                chats = mucs;

                if (listener != null) {
                    listener.handleList(mucs);
                }
            }
        }));
    }

    /**
     * returns the MultiUserChat with given id
     * @param roomId
     * @return
     */
    public MultiUserChat getChatById(String roomId) {
        if(chats == null)
            return null;

        for(MultiUserChat chat : chats) {
            if(roomId.equals(chat.getRoom()))
                return chat;
        }

        return null;
    }

    /**
     * sends a message to a chat room, given roomId
     * @param roomId
     * @param message
     */
    public void sendMessage(String roomId, final String message) {
        MultiUserChat chat = getChatById(roomId);

        sendMessage(chat, message, null);
    }

    /**
     * sends a message to a chat room, given roomId
     * @param roomId
     * @param message
     */
    public void sendMessage(String roomId, final String message, SendMessageRunnable.SendMessageCallback listener) {
        MultiUserChat chat = getChatById(roomId);

        sendMessage(chat, message, listener);
    }

    /**
     * sends a message to a given MultiUserChat
     * @param roomId
     * @param message
     */
    public void sendMessage(MultiUserChat chat, final String message, final SendMessageRunnable.SendMessageCallback listener) {
        if(chat == null)
            return;

        executor.submit(new SendMessageRunnable(connection, chat, message, new SendMessageRunnable.SendMessageCallback() {
            @Override
            public void sent() {
                if (listener != null)
                    listener.sent();
            }

            @Override
            public void failed() {
                if (listener != null)
                    listener.failed();
            }
        }));
    }
}

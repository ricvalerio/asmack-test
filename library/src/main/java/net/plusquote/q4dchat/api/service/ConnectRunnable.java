package net.plusquote.q4dchat.api.service;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 08-12-2014.
 */
public class ConnectRunnable implements Runnable {
    private XMPPTCPConnection connection;

    public ConnectRunnable(XMPPTCPConnection connection) {
        this.connection = connection;
    }

    @Override
    public void run() {
        try {
            connection.connect();
            connection.login();
        } catch (Exception ex) {
            ex.printStackTrace();
            connection = null;
        }
    }
}

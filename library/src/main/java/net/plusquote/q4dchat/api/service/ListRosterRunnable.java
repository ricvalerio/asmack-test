package net.plusquote.q4dchat.api.service;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 08-12-2014.
 */
public class ListRosterRunnable implements Runnable {
    private XMPPTCPConnection connection;
    private ListRosterCallback listener;

    public static interface ListRosterCallback {
        public void handleList(ArrayList<RosterEntry> rooms);
    }

    public ListRosterRunnable(XMPPTCPConnection connection, ListRosterCallback listener) {
        this.connection = connection;
        this.listener = listener;
    }

    @Override
    public void run() {
        if(listener == null)
            return;

        ArrayList<RosterEntry> rosterResult = new ArrayList();

        if(!connection.isConnected())
            listener.handleList(rosterResult);

        Collection<RosterEntry> roster = connection.getRoster().getEntries();

        for(RosterEntry entry : roster) {
            rosterResult.add(entry);
        }

        listener.handleList(rosterResult);
    }
}

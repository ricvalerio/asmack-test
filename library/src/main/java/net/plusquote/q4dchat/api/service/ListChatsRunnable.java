package net.plusquote.q4dchat.api.service;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 08-12-2014.
 */
public class ListChatsRunnable implements Runnable {
    private XMPPTCPConnection connection;
    private ListChatsCallback listener;
    private String service;
    private String screenname;

    public static interface ListChatsCallback {
        public void handleList(ArrayList<MultiUserChat> chats);
    }

    public ListChatsRunnable(XMPPTCPConnection connection, String service, String screenname, ListChatsCallback listener) {
        this.connection = connection;
        this.listener = listener;
        this.service = service;
        this.screenname = screenname;
    }

    @Override
    public void run() {
        if(listener == null)
            return;

        ArrayList<MultiUserChat> chatsResult = new ArrayList();

        if(!connection.isConnected())
            listener.handleList(chatsResult);

        MultiUserChatManager chatManager = MultiUserChatManager.getInstanceFor(connection);
        Collection<HostedRoom> rooms = null;

        try {
            rooms = chatManager.getHostedRooms(service);
        } catch (Exception e) {
            e.printStackTrace();
            listener.handleList(chatsResult);
        }


        for(HostedRoom room : rooms) {
            List<HostedRoom> allRooms = null;
            try {
                allRooms = chatManager.getHostedRooms(room.getJid());
            } catch (Exception e) {
                e.printStackTrace();
            }

            for(HostedRoom simpleRoom : allRooms) {
                MultiUserChat chat = chatManager.getMultiUserChat(simpleRoom.getJid());
                if(!chat.isJoined()) {
                    DiscussionHistory history = new DiscussionHistory();
                    history.setMaxChars(1024);
                    try {
                        chat.join(screenname);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                chatsResult.add(chat);
            }
        }

        listener.handleList(chatsResult);
    }
}

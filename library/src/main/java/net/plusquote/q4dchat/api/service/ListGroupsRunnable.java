package net.plusquote.q4dchat.api.service;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 08-12-2014.
 */
public class ListGroupsRunnable implements Runnable {
    private XMPPTCPConnection connection;
    private ListRoomsCallback listener;
    private String service;

    public static interface ListRoomsCallback {
        public void handleList(ArrayList<HostedRoom> rooms);
    }

    public ListGroupsRunnable(XMPPTCPConnection connection, String service, ListRoomsCallback listener) {
        this.connection = connection;
        this.listener = listener;
        this.service = service;
    }

    @Override
    public void run() {
        if(listener == null)
            return;

        ArrayList<HostedRoom> roomsResult = new ArrayList();

        if(!connection.isConnected())
            listener.handleList(roomsResult);

        MultiUserChatManager chatManager = MultiUserChatManager.getInstanceFor(connection);
        Collection<HostedRoom> rooms = null;

        try {
            rooms = chatManager.getHostedRooms(service);
        } catch (Exception e) {
            e.printStackTrace();
            listener.handleList(roomsResult);
        }

        for(HostedRoom room : rooms) {
            List<HostedRoom> allRooms = null;
            try {
                allRooms = chatManager.getHostedRooms(room.getJid());
            } catch (Exception e) {
                e.printStackTrace();
            }

            for(HostedRoom simpleRoom : allRooms) {
                 roomsResult.add(simpleRoom);
            }
        }

        listener.handleList(roomsResult);
    }
}

package net.plusquote.q4dchat;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import net.plusquote.q4dchat.api.service.ListChatsRunnable;
import net.plusquote.q4dchat.api.service.ListGroupsRunnable;
import net.plusquote.q4dchat.api.Q4DChatApi;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MUCNotJoinedException;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Ricardo Valério (ricardo.valerio@virgo.hu) on 08-12-2014.
 */
public class Q4DChat {
    private final static Q4DChat instance = new Q4DChat();
    private Q4DChatApi client;
    private HashMap<String, ArrayList<MyMessage>> chatRooms;
    private HashMap<String, Date> lastReadDates;

    private OnMessage messageListener;
    private Context context;
    private Handler handler;
    protected Q4DChat.OnConnect connectListener;

    protected String username;
    protected String password;
    protected String screenname;

    /**
     * interface for connection related callbacks
     */
    public interface OnConnect {
        public void connected();
        public void authenticated();
        public void failed();
        public void reconnecting();
        public void closed();
    }

    /**
     * interface for message listening
     */
    public interface OnMessage {
        public void received(MultiUserChat chat, Message message);
    }


    private Q4DChat() {
        client = new Q4DChatApi();
        handler = new Handler();
        chatRooms = new HashMap();
        lastReadDates = new HashMap();
    }

    public static Q4DChat getInstance() {
        return instance;
    }

    public boolean isConnected() {
        return client.isConnected();
    }

    /**
     * attempts to connect and authenticate the user
     * @param username
     * @param password
     * @param listener
     */
    public void connect(String username, String password, String screenname, OnConnect listener) {
        this.username = username;
        this.password = password;
        this.screenname = screenname;

        client.setup(Config.HOST, Config.PORT, Config.SERVICE)
                .setOnConnectListener(new MyConnectionListener(handler, listener))
                .connect(username, password, screenname);
    }

    public void setConnectionListener(OnConnect listener) {
        connectListener = listener;
    }

    /**
     * gets all cached messages for a given MUC
     * @param roomId
     * @return
     */
    public ArrayList<MyMessage> getMessages(String roomId) {
        if(chatRooms.get(roomId) == null)
            chatRooms.put(roomId, new ArrayList<MyMessage>());

        return chatRooms.get(roomId);
    }

    public int getNumUnread(String roomId) {
        Date date = lastReadDates.get(roomId);
        if(date == null)
            return getMessages(roomId).size();

        int numUnread = 0;
        Date messageDate = null;
        ArrayList<MyMessage> messages = getMessages(roomId);
        for(MyMessage m : messages) {
            Message message = m.getMessage();

            DelayInformation inf = null;
            try {
                inf = message.getExtension("delay", "urn:xmpp:delay");
                messageDate = inf.getStamp();
            } catch (Exception e) {
                messageDate = null;
            }

            if(messageDate == null || messageDate.after(date)) {
                numUnread++;
            }
        }

        return numUnread;
    }

    /**
     * list rooms
     * @param listener
     */
    public void listChats(final ListGroupsRunnable.ListRoomsCallback listener) {
        client.listRooms(new ListGroupsRunnable.ListRoomsCallback() {
            @Override
            public void handleList(final ArrayList<HostedRoom> rooms) {
                if (listener != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.handleList(rooms);
                        }
                    });
                }
            }
        });
    }

    /**
     * send a message to a MUC
     * @param roomId
     * @param message
     */
    public void sendMessage(String roomId, String message) {
        if(message.isEmpty())
            return;

        client.sendMessage(roomId, message);
    }

    /**
     * set the listener for chat messages
     * @param listener
     */
    public void setOnMessageListener(OnMessage listener) {
        messageListener = listener;
    }

    /**
     * adds a message to the message cache
     * @param room
     * @param message
     */
    private void addMessage(String room, Message message) {
        if(chatRooms.get(room) == null)
            chatRooms.put(room, new ArrayList<MyMessage>());

        if(message.getBody().isEmpty())
            return;

        ArrayList<MyMessage> messages = chatRooms.get(room);
        for(MyMessage m : messages) {
            if(m.getMessage().getPacketID() != null && m.getMessage().getPacketID().equals(message.getPacketID())) {
                m.getMessage().addExtension(message.getExtension("delay", "urn:xmpp:delay"));
                return;
            }
        }

        Log.d("miau", "miau "+message.toXML());

        messages.add(0, new MyMessage(message));
    }

    /**
     * sets up the message listener for the user MUCs
     */
    private void setupChatListeners() {
        client.getChatRooms(new ListChatsRunnable.ListChatsCallback() {
            @Override
            public void handleList(ArrayList<MultiUserChat> chats) {
                for (MultiUserChat chat : chats) {
                    lastReadDates.clear();
                    lastReadDates.put(chat.getRoom(), new Date());

                    try {
                        Message message;

                        while( (message = chat.nextMessage()) != null ){
                            addMessage(chat.getRoom(), message);
                        }
                    } catch (MUCNotJoinedException e) {
                        e.printStackTrace();
                    }

                    chat.addMessageListener(new MyMessageListener(chat));
                }
            }
        });
    }


    private class MyMessageListener implements MessageListener {
        private MultiUserChat chat;

        public MyMessageListener(MultiUserChat chat) {
            this.chat = chat;
        }

        @Override
        public void processMessage(final Message message) {
            addMessage(chat.getRoom(), message);

            if(messageListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        messageListener.received(chat, message);
                    }
                });
            }
        }
    }


    public class MyConnectionListener implements ConnectionListener {
        //private Q4DChat.OnConnect connectListener;
        private Handler handler;

        public MyConnectionListener(Handler handler, Q4DChat.OnConnect listener) {
            this.handler = handler;
            connectListener = listener;
        }

        @Override
        public void connected(XMPPConnection xmppConnection) {
            if(connectListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectListener.connected();
                    }
                });
            }
        }

        @Override
        public void authenticated(XMPPConnection xmppConnection) {
            setupChatListeners();

            if(connectListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        chatRooms = new HashMap();
                        connectListener.authenticated();
                    }
                });
            }

        }

        @Override
        public void connectionClosed() {
            if(connectListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectListener.closed();
                    }
                });
            }

        }

        @Override
        public void connectionClosedOnError(Exception e) {
            if(connectListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectListener.closed();
                    }
                });
            }

        }

        @Override
        public void reconnectingIn(int i) {
            if(connectListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectListener.reconnecting();
                    }
                });
            }

        }

        @Override
        public void reconnectionSuccessful() {
            if(connectListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectListener.connected();
                    }
                });
            }

        }

        @Override
        public void reconnectionFailed(Exception e) {
            if(connectListener != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        connectListener.failed();
                    }
                });
            }
        }
    }

}

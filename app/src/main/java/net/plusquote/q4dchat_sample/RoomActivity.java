package net.plusquote.q4dchat_sample;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import net.plusquote.q4dchat.MyMessage;
import net.plusquote.q4dchat.Q4DChat;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.util.ArrayList;


public class RoomActivity extends ActionBarActivity {

    private ListView lvChatEntries;
    private EditText etMessage;
    private Button btSubmit;
    private Q4DChat client;

    private MessageAdapter adapter;
    private String roomId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_room);

        lvChatEntries = (ListView) findViewById(R.id.lv_chat_entries);
        etMessage = (EditText) findViewById(R.id.et_message);
        btSubmit = (Button) findViewById(R.id.bt_submit);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Intent intent = getIntent();
        roomId = intent.getStringExtra("roomId");

        setupOldMessages(roomId);

        getClient().setOnMessageListener(new Q4DChat.OnMessage() {
            @Override
            public void received(MultiUserChat chat, Message message) {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private Q4DChat getClient() {
        if(client == null)
            client = Q4DChat.getInstance();

        return client;
    }


    private void sendMessage() {
        String text = etMessage.getText().toString().trim();

        if(text == null)
            return;

        getClient().sendMessage(roomId, text);

        etMessage.setText("");
    }


    private void setupOldMessages(String roomId) {
        ArrayList<MyMessage> messages = getClient().getMessages(roomId);

        adapter = new MessageAdapter(this, R.layout.message_item, messages);
        lvChatEntries.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    /**
     * simple adapter for the chat messages
     */
    private class MessageAdapter extends ArrayAdapter<MyMessage> {
        private ArrayList<MyMessage> messages;

        public MessageAdapter(Context context, int resource, ArrayList<MyMessage> objects) {
            super(context, resource, objects);

            messages = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh = null;

            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.message_item, parent, false);

                vh = new ViewHolder();
                vh.tvFrom = (TextView) convertView.findViewById(R.id.tv_from);
                vh.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
                vh.tvMessage = (TextView) convertView.findViewById(R.id.tv_message);
                convertView.setTag(vh);
            }

            if(vh == null) {
                vh = (ViewHolder) convertView.getTag();
            }

            MyMessage message = getItem(position);

            vh.tvFrom.setText(message.getPlayerId());

            CharSequence howLongAgo = message.getHowLongAgo();
            vh.tvDate.setText(howLongAgo);

            vh.tvMessage.setText(message.getBody());

            return convertView;
        }

        private class ViewHolder {
            private TextView tvFrom;
            private TextView tvDate;
            private TextView tvMessage;
        }
    }

}

package net.plusquote.q4dchat_sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import net.plusquote.q4dchat.Q4DChat;
import net.plusquote.q4dchat.api.service.ListGroupsRunnable;

import org.jivesoftware.smackx.muc.HostedRoom;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    private ListView lvChannels;
    private Q4DChat q4dclient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        lvChannels = (ListView) findViewById(R.id.lv_channels);
    }

    @Override
    public void onResume() {
        super.onResume();

        q4dclient = Q4DChat.getInstance();

        if(!q4dclient.isConnected())
            q4dclient.connect("85914", "57e5090f-bb79-4525-abfa-5e56069536c9", "teste", new ChatListener(q4dclient));
            //q4dclient.connect("86201", "5b157232-66a9-45df-a655-e6dfc9619082", new ChatListener(q4dclient));
    }

    private class ChatListener implements Q4DChat.OnConnect {
        private Q4DChat client;

        public ChatListener(Q4DChat client) {
            this.client = client;
        }

        @Override
        public void connected() {
            Toast.makeText(getBaseContext(), "connected", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void authenticated() {
            Toast.makeText(getBaseContext(), "authenticated", Toast.LENGTH_SHORT).show();

            q4dclient.listChats(new ListGroupsRunnable.ListRoomsCallback() {
                @Override
                public void handleList(ArrayList<HostedRoom> rooms) {
                    ArrayList<String> items = new ArrayList<String>();

                    for (HostedRoom group : rooms) {
                        items.add(group.getJid());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, items);
                    lvChannels.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });

            lvChannels.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getBaseContext(), RoomActivity.class);

                    String roomId = ((ArrayAdapter<String>) lvChannels.getAdapter()).getItem(position);
                    intent.putExtra("roomId", roomId);
                    startActivity(intent);
                }
            });
        }

        @Override
        public void failed() {
            Toast.makeText(getBaseContext(), "failed to connect", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void reconnecting() {
            Toast.makeText(getBaseContext(), "reconnecting", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closed() {
            Toast.makeText(getBaseContext(), "connectiong closed", Toast.LENGTH_SHORT).show();
        }
    }
}
